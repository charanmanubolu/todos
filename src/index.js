let inputCard = document.querySelector("#submit");
let ulElements = document.getElementById("itemList");
let downArrow = document.getElementById("down-arrow");
let numberOfItems = document.getElementById('numberOfItems');
let count = 0;
let ischecked = false;

let all = document.getElementById("All");
let active = document.getElementById("Active");
let completed = document.getElementById("completed");
let clearCompleted = document.getElementById("clearCompleted");
let foot = document.getElementById("footer-container");

inputCard.addEventListener("submit", addItemToList);

ulElements.addEventListener("dblclick", (e) => {
    let li = e.target.closest("li");
    if (li) {
        editItems(li);
    }
});

downArrow.addEventListener("click", () => {
    ischecked = !ischecked;
    for (let index = 0; index < ulElements.children.length; index++) {
        let checkbox = ulElements.children[index].querySelector("input");
        let label = ulElements.children[index].querySelector("label");

        if (ischecked) {
            checkbox.checked = true;
            label.classList.add("line-through");
        } else {
            checkbox.checked = false;
            label.classList.remove("line-through");
        }
    }
    count = ischecked ? 0 : ulElements.children.length;
    countitems(count);
});

all.addEventListener('click', allItems);
active.addEventListener("click", activeItems);
completed.addEventListener("click", completedItems);
clearCompleted.addEventListener('click', clearComplete);

function addItemToList(e) {
    e.preventDefault();

    let ul = document.querySelector(".list-items");
    const input = inputCard.firstElementChild.value.trim();

    if (!input){
      return;
    }

    const li = document.createElement("li");
    const checkbox = document.createElement("input");
    const label = document.createElement("label");
    checkbox.setAttribute("type", "checkbox");
    checkbox.className = "ml-10 mr-5";
    li.className = "border-b-2 py-1 my-4 px-0";

    let button = document.createElement("button");
    button.textContent = "X";
    button.className = "float-right font-bold mr-3 text-red-700 deleteButton opacity-0";

    button.addEventListener("click", () => {
        if (!checkbox.checked) {
          count--;
        li.remove();
        countitems(count);
        }
    });

    li.addEventListener("mouseenter", () => {
        button.classList.remove("opacity-0");
        button.classList.add("opacity-1");
    });
    li.addEventListener("mouseleave", () => {
        button.classList.remove("opacity-1");
        button.classList.add("opacity-0");
    });

    label.textContent = input;

    checkbox.addEventListener("click", () => {
        if (checkbox.checked) {
            label.classList.add("line-through");
            count--;
        } else {
            label.classList.remove("line-through");
            count++;
        }
        countitems(count);
    });

    li.append(checkbox);
    li.append(label);
    li.append(button);
    ul.append(li);

    count++;
    countitems(count);

    inputCard.firstElementChild.value = "";

    foot.classList.remove("hidden");
    downArrow.classList.remove('hidden');
}

function editItems(li) {
    let label = li.querySelector("label");
    let checkbox = li.querySelector("input");
    let button = li.querySelector("button");

    let currentText = label.textContent;
    let input = document.createElement("input");

    input.type = "text";
    input.value = currentText;
    label.parentNode.replaceChild(input, label);

    input.focus();
    input.className = "w-full";
    checkbox.classList.add("hidden");
    button.classList.add("opacity-0");

    input.addEventListener("keypress", (e) => {
        if (e.key === "Enter") {
            label.textContent = input.value;

            input.parentNode.replaceChild(label, input);
            checkbox.classList.remove("hidden");
            button.classList.add("opacity-0");
        }
    });
    input.addEventListener("blur", () => {
        label.textContent = input.value;
        input.parentNode.replaceChild(label, input);
        checkbox.classList.remove("hidden");
        button.classList.add("opacity-0");
    });
}

function countitems(num) {
    if (num >= 0) {
        numberOfItems.textContent = `${num} items left`;
    }
}

function allItems() {
    let liElements = ulElements.children;
    Array.from(liElements).forEach(li => {
        li.classList.remove('hidden');
    });
}

function activeItems() {
    let liElements = ulElements.children;
    Array.from(liElements).forEach(li => {
        let checkbox = li.querySelector('input');
        if (checkbox.checked) {
            li.classList.add('hidden');
        } else {
            li.classList.remove('hidden');
        }
    });
}

function completedItems() {
    let liElements = ulElements.children;
    Array.from(liElements).forEach(li => {
        let checkbox = li.querySelector('input');
        if (checkbox.checked) {
            li.classList.remove('hidden');
        } else {
            li.classList.add('hidden');
        }
    });
}

function clearComplete() {
    let liElements = ulElements.children;
    Array.from(liElements).forEach(li => {
        let checkbox = li.querySelector('input');
        if (checkbox.checked) {
            li.remove();
        }
    });
    countitems(count);
}
